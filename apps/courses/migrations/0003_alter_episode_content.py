# Generated by Django 3.2.16 on 2023-06-15 14:06

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0002_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='episode',
            name='content',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
    ]
